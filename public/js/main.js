let validatorView = new ValidatorView();
let validatorModel = new ValidatorModel();
let validatorController = new ValidatorController(validatorView, validatorModel);

let galleryView = new GalleryView();
let galleryModel = new GalleryModel();
let galleryController = new GalleryController(galleryView, galleryModel);

let userView = new UserView();
let userModel = new UserModel();
let userController = new UserController(userView, userModel);


let loginView = new LoginView();
let loginModel = new LoginModel();
let loginController = new LoginController(
    loginView,
    loginModel,
    validatorController,
    galleryController,
    userController
);
loginController.initComponent();