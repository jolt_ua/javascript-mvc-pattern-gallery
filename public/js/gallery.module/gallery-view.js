class GalleryView {
    constructor() {
        this.galleryView = {
            wrapper: document.getElementById('gallery-wrapper'),
            nameSortSelector: document.getElementById('dropdown-name'),
            dateSortSelector: document.getElementById('dropdown-date'),
            viewContainer: document.getElementById('gallery-view'),
            addCard: document.getElementById('addCard'),
        };
        this.formView = {
            wrapper: document.getElementById('cardCreate'),
            nameInput: document.getElementById('carName'),
            imgInput: document.getElementById('carImg'),
            dateInput: document.getElementById('carDateView'),
            descriptionInput: document.getElementById('carDescription'),
            btnCreate: document.getElementById('createCarBtn'),
            btnEdit: document.getElementById('editCarBtn'),
            formTitle: document.getElementById('create-h1'),
            id: document.getElementById('id-input'),
        }
    }

    renderDataView(data) {
		let htmlResult = '';
		data.forEach(item => htmlResult += this.buildString(item));
		this.galleryView.viewContainer.innerHTML = htmlResult;
    }
    
    buildString(item) {
		return `<div class="col-md-4">
					<div class="card mb-4 box-shadow">
						<img class="card-img-top" 
							data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" 
							alt="${item.name}"
							src="${item.url}" 	
							data-holder-rendered="true"
							style="height: 225px; 
							width: 100%; display: block;">
						<div class="card-body">
							<h3>${item.name}</h3>
							<p class="card-text">${item.description}</p>
								<div class="d-flex justify-content-between align-items-center">
									<div class="btn-group">
										<button type="button" class="btn btn-outline-secondary">View</button>
										<button type="button" class="btn btn-outline-secondary" data-edit="${item.id}">Edit</button>
									</div>
									<a href="#" class="btn btn-danger" data-id="${item.id}">Удалить</a>
									<small class="text-muted">${item.date}</small>
								</div>
						</div>
					</div>
				</div>`
    }
    
    toggleButtonText(event) {
		event.currentTarget.querySelector("button").innerHTML = event.target.innerText
    }

    getDataType(event) {
        return event.target.getAttribute("data-type");
	}

	getDataAtrrId(event) {
		if (!event.target.getAttribute("data-id")) {
			return;
		}
		return event.target.getAttribute("data-id");
	}

	getDataAtrrEdit(event) { //join to getDataAtrrId
		if (!event.target.getAttribute("data-edit")) {
			return;
		}
		return event.target.getAttribute("data-edit");
	}

	setValueToCardEdit(card) {
		this.formView.imgInput.value = card.url; 
        this.formView.nameInput.value = card.name;
        this.formView.descriptionInput.value = card.description; 
		this.formView.dateInput.value = utils.formatDateMilliToIso(+card.date);
		this.formView.id.value = card.id 
	}
	
	showCreationForm() {
		this.formView.wrapper.classList.add('show');
		this.formView.formTitle.innerHTML = 'Создать карточку';
		this.formView.btnCreate.classList.remove('hide');
		this.formView.btnEdit.classList.add('hide');
		this.formView.id.classList.add('hide');
	}

	showEditableForm() {
		this.formShow();
		this.formView.formTitle.innerHTML = 'Редактировать карточку';
		this.formView.btnCreate.classList.add('hide');
		this.formView.btnEdit.classList.remove('hide');
	}

	getInputValues() {
		// let card = {};
		return {
			id: this.formView.id.value,
			url: this.formView.imgInput.value,
			name: this.formView.nameInput.value,
			description: this.formView.descriptionInput.value,
			date: utils.formatDateIsoToMilli(this.formView.dateInput.value)
		}
    }

    formShow() {
		this.formView.wrapper.classList.remove('hide');
		this.formView.wrapper.classList.add('show');
	}
	
	formHide() {
		this.formView.wrapper.classList.remove('show');
		this.formView.wrapper.classList.add('hide');
	}
	
	show() {
		this.galleryView.wrapper.classList.remove('hide');
	}
					
	hide() {
		this.galleryView.wrapper.classList.add('hide');
	}
}
