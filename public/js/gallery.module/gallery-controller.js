class GalleryController {
    constructor(galleryView, galleryModel) {
        this.view = galleryView;
		this.model = galleryModel;
		this.data = [];
    }

    init() {  
		this.view.hide();
		this.view.formHide();
		this.initListeners();
		this.model.getData().then(data => this.view.renderDataView(data));
    }
    
    initListeners() {
		this.view.galleryView.nameSortSelector.addEventListener('click', this.getSortingType.bind(this));
		this.view.galleryView.dateSortSelector.addEventListener('click', this.getSortingType.bind(this));
		this.view.galleryView.addCard.addEventListener('click', this.showAddCard.bind(this));
		this.view.galleryView.viewContainer.addEventListener('click', this.deleteOneCard.bind(this));
		this.view.galleryView.viewContainer.addEventListener('click', this.showEditCardForm.bind(this));
		this.view.formView.btnCreate.addEventListener('click', this.createNewCard.bind(this));
		this.view.formView.btnEdit.addEventListener('click', this.editCard.bind(this));
	}

	getSortingType(event) {
		event.preventDefault();
		this.view.toggleButtonText(event);
		let sortingType = this.view.getDataType(event);
		if (sortingType) {
			this.model.sortingArray(sortingType)
				.then(result => this.view.renderDataView(utils.format(result)))
			// this.view.renderDataView(this.model.sortingArray(sortingType));
		};
	}

	showAddCard() {
		this.view.hide();
		this.view.showCreationForm()
	}

	showEditCardForm(event) {
		event.preventDefault();
		if (this.view.getDataAtrrEdit(event)) { 
			this.model.findEditableCard(this.view.getDataAtrrEdit(event))
				.then(result =>
					this.view.setValueToCardEdit(result));
					this.view.hide();
					this.view.showEditableForm();
			}
	}

	deleteOneCard(event) { //cделать переменную
		event.preventDefault();
		if (this.view.getDataAtrrId(event)) {
			this.model.deleteCard(this.view.getDataAtrrId(event)).then(data => {
				return this.view.renderDataView(data);
			});
		};
	}

	createNewCard(event) {
		event.preventDefault();
		this.model.createCard(this.view.getInputValues()).then(result => {
			this.view.renderDataView(result);
			this.view.formHide();
			this.view.show();
		});
	}

	editCard(event) {
		event.preventDefault();
		this.model.editCard(this.view.getInputValues()).then(result => {
			this.view.renderDataView(result);
			this.view.formHide();
			this.view.show();
		})
    }

	showGallery() {
		this.view.show();
	}

	hideGallery() {
		this.view.hide();
	}

	
}
