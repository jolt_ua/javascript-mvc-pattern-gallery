class GalleryModel {
    constructor() {
		this.data = [];
	}

    getData() { 
		let url = 'http://localhost:3000/cars'
		return this.getServerDataFetch(url, {}).then((data) => {
			return this.data = utils.format(data);
		}).catch((error) => {
            console.log(error);
        });
	}

	deleteCard(itemId) {
		let url = `http://localhost:3000/cars/${itemId}`
        return this.getServerDataFetch(url, {method: 'delete'}).then((res) => {
			if (res) {
				return this.getData();
            }
        }).catch((error) => {
			console.log(error);
        });
	}

	createCard(card) {
		let url = 'http://localhost:3000/cars'
		return this.getServerDataFetch(url, this.setOptions('post', card))
			.then((res) => {
				if (res) {
					return this.getData();
				}
			}).catch((error) => {
				console.log(error);
        });
	}

	editCard(card) {
		let url = `http://localhost:3000/cars/${card.id}`;
		return this.getServerDataFetch(url, this.setOptions('put', card))
			.then((res) => {
				if (res) {
					return this.getData();
				}
			}).catch((error) => {
				console.log(error);
        });
	}

    getServerDataFetch(url, options) {
		return fetch(url, options).then((response) => {
			return response.json();
		});
	}

	sortingArray(sortingType) {
		let url = 'http://localhost:3000/cars'
		return this.getServerDataFetch(url, {}).then((data) => {
			switch (sortingType) {
				case 'asc':
					return data.sort(function (a, b) {
						return a.name > b.name ? 1 : -1;
					});
				case 'dasc':
					return data.sort(function (a, b) {
						return a.name < b.name ? 1 : -1
					});
				case 'new':
					return data.sort(function (a, b) {
						return +a.date > +b.date ? 1 : -1;
					});
				case 'old':
					return data.sort(function (a, b) {
						return +a.date < +b.date ? 1 : -1
					});
			}
		}).catch((error) => {
            console.log(error);
        });
		
	}

	findEditableCard(id) { // Через get на id для одной карточки
		let url = 'http://localhost:3000/cars'
		return this.getServerDataFetch(url, {}).then((data) => {
			let res = data.find(item => item.id === +id);
			return res
		}).catch((error) => {
            console.log(error);
        });
		
	}

	setOptions(method, card) {
		return {
			method,
            headers: {
                'Content-Type': 'application/json'
			},
            body: JSON.stringify({
				url : `${card.url}`,
                name : `${card.name}`,
                description : `${card.description}`,
                date : `${card.date}`
            })
        }
    }
}
