class ValidatorController {
    constructor(validatorView, validatorModel) {
        this.view = validatorView;
        this.model = validatorModel;
        this.loginStatus = '';
    }

    isValid(user) {
        return new Promise ((resolve, reject) => {
            if (user.login.length === 0) {
                reject({
                    status: false,
                    msg: 'Поле логин не может быть пустым'
                })
            }
            if (!this.isTrueEmail(user)) {
                reject({
                    status: false,
                    msg: 'Не верный формат email'
                })
            }
            if (user.password.length === 0) {
                reject({
                    status: false,
                    msg: 'Пароль не должен быть пустым'
                })
            }
            if (user.password.length < 8) {
                reject({
                    status: false,
                    msg: 'Пароль должен быть 8 и более символов'
                })
            }
            let url = `http://localhost:3000/user` //перенести в модель
            this.checkUserAccess(url, this.setOptions(user, 'post')).then((res) => {
                if (res) {
                    localStorage.setItem('auth_token', 'true')
                    resolve({
                        status: true,
                        msg: 'Вы успешно вошли в систему'
                    })
                }
            }).catch((error) => {
                if(error == 'Unknown error') {
                    reject({ 
                        status: false,
                        msg: 'Повторите вашу попытку позже'
                    })
                }
                reject({
                    status: false,
                    msg: 'Такого пользователя не существует'
                })
            });
        })
        
    }

    isTrueEmail(user) {
        this.emailRegExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return this.emailRegExp.test(user.login)
    }

    setOptions(user, method) {
        return {
            method,
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                login: `${user.login}`,
                password: `${user.password}`
            })
        }
    }

    checkUserAccess(url, options) {
		return new Promise((resolve, reject) => {
			fetch(url, options).then((responce) => {
				if (+responce.status === 200) {
					resolve(responce.json())
				} else if (+responce.status === 401) {
					reject(responce.statusText)
				} else {
					reject(new Error('Unknown error'))
				}
			})
		})
	}
}