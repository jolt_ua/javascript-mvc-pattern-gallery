class LoginView {
    constructor() {
        this.loginView = {
            wrapper: document.getElementById('login-wrapper'),
            msgAlertBox: document.getElementById('msgBox'),
            emailInput: document.getElementById('inputEmail'),
            passwordInput: document.getElementById('inputPassword'),
            btnSubmit: document.getElementById('submitBtn'),
            gallery: document.getElementById('gallery'),
            userInfo: document.getElementById('user-info'),
            exitBtn: document.getElementById('exit'),
            rememberUser: document.getElementById('remember')
        }
        this.navBar = [
            this.loginView.gallery,
            this.loginView.userInfo,
            this.loginView.exitBtn
        ];
    }

    hideMsgBox() {
        this.loginView.msgAlertBox.classList.add('hide');
    }

    markActiveRoute(btn){
        if (btn === 'gallery') {
            this.loginView.gallery.classList.add('active');
		    this.loginView.userInfo.classList.remove('active');
        } else {
            this.loginView.gallery.classList.remove('active');
		    this.loginView.userInfo.classList.add('active');
        }
    }

    resetLoginForm() {
            this.loginView.emailInput.value = '';
			this.loginView.passwordInput.value = ''
    }

    getUserInputData() {
		return {
			login: this.loginView.emailInput.value,
			password: this.loginView.passwordInput.value,
		}
    }
    
    showAlertMsg(msg) {
		this.loginView.msgAlertBox.classList.remove('hide');
		this.loginView.msgAlertBox.innerHTML = msg;
		setTimeout(() => {
			this.loginView.msgAlertBox.classList.add('hide');
			this.loginView.msgAlertBox.innerHTML = '';
		}, 2000)
	}

	showSuccesMsg(msg) {
		this.loginView.msgAlertBox.classList.remove('hide');
		this.loginView.msgAlertBox.classList.remove('alert-danger');
		this.loginView.msgAlertBox.classList.add('alert-success');
		this.loginView.msgAlertBox.innerHTML = msg;
		setTimeout(() => {
			this.loginView.msgAlertBox.classList.add('hide');
			this.loginView.msgAlertBox.classList.remove('alert-success');
			this.loginView.msgAlertBox.classList.add('alert-danger');
		}, 2000)
	}

    show() {
        this.loginView.wrapper.classList.remove('hide');
    }

    hide() {
        this.loginView.wrapper.classList.add('hide');
    }

    showNavBar() {
        this.navBar.map(item => item.classList.remove('hide'));
    }

    hideNavBar() {
        this.navBar.map(item => item.classList.add('hide'));
    }
}