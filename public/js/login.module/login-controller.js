class LoginController {
    constructor(loginView,
                loginModel,
                validatorController,
                galleryController,
                userController) {
        this.view = loginView;
        this.model = loginModel;
        this.validator = validatorController;
        this.gallery = galleryController;
        this.userDetails = userController; 
        this.isLoginValid = false;
    }

    initComponent() { //lazy
        this.userDetails.init(); 
        this.view.hideMsgBox();
        this.gallery.init();
        this.initListeners();
        if(!localStorage.getItem('auth_token')) {
			this.view.hideNavBar();
		} else {
            this.view.showNavBar();
            this.view.markActiveRoute('gallery');
			this.view.hide();
			this.gallery.showGallery();
        }
        
    }

    initListeners() {
		this.view.loginView.gallery.addEventListener('click', this.showGallery.bind(this));
		this.view.loginView.userInfo.addEventListener('click', this.showUserDetails.bind(this));
        this.view.loginView.exitBtn.addEventListener('click', this.loggout.bind(this));
        this.view.loginView.btnSubmit.addEventListener('click', this.signIn.bind(this));
    }

    signIn(event) {
        event.preventDefault();
			this.validator.isValid(this.view.getUserInputData()).then(res => {
                if (res) {
                    this.isLoginValid = this.validator.loginStatus;
                    this.view.showSuccesMsg(res.msg);
					this.userDetails.setUserData(this.view.getUserInputData())
					setTimeout(() => {
						this.view.showNavBar();
						this.view.markActiveRoute('gallery');
						this.view.hide();
                        this.gallery.showGallery();
                    }, 500);
                }
            }).catch((err) => {
                console.error(err);
                this.view.showAlertMsg(err.msg);
                this.isLoginValid = this.validator.loginStatus;
            });
    }

    showGallery() { //Оптимизировать 
        this.view.markActiveRoute('gallery');
		this.userDetails.hideDetails();
		this.gallery.showGallery();
    }
    
    showUserDetails() {
		this.view.markActiveRoute('userDetails');
		this.userDetails.showDetails();
		this.gallery.hideGallery();
    }
    
    loggout() {
		localStorage.removeItem('auth_token')
		this.userDetails.hideDetails();
        this.gallery.hideGallery();
        this.view.show();
        this.view.hideNavBar();
		if (!this.view.loginView.rememberUser.checked) {
            this.view.resetLoginForm();
		}
	}
    

}