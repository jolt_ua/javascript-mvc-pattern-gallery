class UserView {
    constructor() {
        this.userView = {
            wrapper: document.getElementById('showUser'),
            loginInput: document.getElementById('userLogin'),
            passwordInput: document.getElementById('userPassword'),
            showPassBtn: document.getElementById('showPassword'),
        }
    }

    initListeners() {
        this.userView.showPassBtn.addEventListener('click', this.togglePasswordView.bind(this))
    }

    togglePasswordView(event) {
        event.preventDefault();
        if (!this.isShowPass) {
            this.userView.passwordInput.setAttribute('type', 'text');
            this.userView.showPassBtn.innerText = 'Скрыть пароль';
            this.isShowPass = true;
        } else {
            this.userView.passwordInput.setAttribute('type', 'password');
            this.userView.showPassBtn.innerText = 'Показать пароль';
            this.isShowPass = false;
        }

    }

    setDataToInput(user) {
        this.userView.loginInput.value = user.login;
        this.userView.passwordInput.value = user.password;
    }

    hide() {
        this.userView.wrapper.classList.add('hide');
    }
    
    show() {
        this.userView.wrapper.classList.remove('hide');
    }
}