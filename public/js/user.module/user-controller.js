class UserController {
    constructor(userView, userModel) {
        this.view = userView;
        this.model = userModel;
    }

    init() {
        this.view.hide();
        this.view.initListeners();
    }

    setUserData(user) {
        this.view.setDataToInput(user);
    }

    showDetails() {
        this.view.show();
    }

    hideDetails() {
        this.view.hide();
    }
}