const utils = {
  format: function (data) {
    return data.map(({
      id,
      name,
      url,
      description,
      date
    }) => {
      return {
        id,
        name: this.formatName(name),
        url: this.formatUrl(url),
        description: this.formatDescription(description),
        date: this.formatDateMilliToIso(date)
      }
    })
  },

  formatName: function (name) {
    return name.charAt(0).toUpperCase() + name.slice(1).toLowerCase()
  },

  formatDescription: function (description) {
    return description.length > 26 ?
      `${description.substring(0, 26)}...` :
      `${description}`
  },

  formatUrl: function (url) {
    if (url.startsWith("http://")) return url;
    if (url.startsWith("https://")) return url;
    return "http://" + url;
  },

  formatDateMilliToIso: function (milli) {
    return moment(+milli).format("DD.MM.YYYY")
  },

  formatDateIsoToMilli: function (iso) {
    return moment(`${iso}`, "DD.MM.YYYY").format("x")
  }

}
